package com.mjolnirr.performancetest;

import com.mjolnirr.lib.component.AbstractApplication;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;
import com.mjolnirr.lib.Callback;

import java.util.ArrayList;

public class Calculator extends AbstractApplication {
    private static final int MAX_COUNT = 99;
    private static Integer count;

    private ComponentContext context;

    public static class CallbackA implements Callback<String> {
        public static long startTime;

        public CallbackA() {
        }

        public void run(String res) {
            synchronized(count) {
                count--;

                if (count == 0) {
                    System.out.println("Aaaand I came");
                    System.out.println("Delta is " + (System.currentTimeMillis() - startTime - 10 * 1000));
                } else {
                    System.out.println("" + count + " left");
                }
            }
        }
    }

    @Override
    public void initialize(ComponentContext componentContext) {
        this.context = componentContext;
    }

    public Response calculate(final Request request) throws Exception {
        System.out.println("Context " + request.getContent());

        CallbackA.startTime = System.currentTimeMillis();

        System.out.println("Starting!");
        count = MAX_COUNT;
        Communicator communicator = new HornetCommunicator();

        for (int i = 1 ; i <= MAX_COUNT ; i++) {
             communicator.send(context, "proxym" + i, "execute", new ArrayList<Object>() {{ add(request.getContent());}}, new CallbackA());
        }

        return new Response("");
    }
}
