package com.mjolnirr.performancetest;

import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CalculatorHelper extends AbstractModule {
    @Override
    public void initialize(ComponentContext componentContext) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public static String execute(String expression) {
        List<String> parts = splitExpression(expression);
        String operation = null;
        Stack<Double> vals = new Stack<Double>();

        for (String part : parts) {
            if (part.equals("+") || part.equals("*") || part.equals("-") || part.equals("/")) {
                operation = part;
            } else {
                vals.push(Double.parseDouble(part));
            }
        }

        getComp(operation, vals);
        return Double.toString(vals.pop());
    }

    private static List<String> splitExpression(String expression) {
        List<String> parts = new ArrayList<String>();
        String[] chars = expression.split("");

        String part = "";
        for (String ch : chars) {
            if (ch.equals("+") || ch.equals("*") || ch.equals("-") || ch.equals("/")) {
                parts.add(ch);
                if (!part.isEmpty()) {
                    parts.add(part);
                    part = "";
                }
            } else if (!ch.equals(" ") && !ch.equals("")) {
                part += ch;
            }
        }

        parts.add(part);
        return parts;
    }

    private static void getComp(String operation, Stack<Double> values) {
        double second = values.pop();
        double first = values.pop();

        if (operation.equals("+")) {
            values.push(first + second);
        } else if (operation.equals("*")) {
            values.push(first * second);
        } else if (operation.equals("-")) {
            values.push(first - second);
        } else if (operation.equals("/")) {
            values.push(first / second);
        }
    }
}
