package com.mjolnirr.performancetest;

import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Proxym1 extends AbstractModule {
    private ComponentContext context;

    @Override
    public void initialize(ComponentContext componentContext) {
        context = componentContext;
    }

    public String execute(final String expression) {
        try {
            Communicator communicator = new HornetCommunicator();
            return communicator.sendSync(context, "proxym2", "execute", new ArrayList<Object>() {{ add(expression); }}, String.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
