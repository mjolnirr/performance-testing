package com.mjolnirr.performancetest;

import com.mjolnirr.lib.component.AbstractModule;
import com.mjolnirr.lib.component.ComponentContext;
import com.mjolnirr.lib.msg.Communicator;
import com.mjolnirr.lib.msg.HornetCommunicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Proxym{n} extends AbstractModule {
    private ComponentContext context;

    @Override
    public void initialize(ComponentContext componentContext) {
        context = componentContext;
    }

    public String execute(final String expression) {
         try {
             Thread.sleep(10 * 1000);
         } catch (Exception e) {
             e.printStackTrace();
         }
         return "";
    }
}
