mkdir jars

for i in {1..99}; do
  let m=$i+1;
  cp -r proxy-template proxym$i;
  mv proxym$i/src/main/java/com/mjolnirr/performance-test/Proxym{n}.java proxym$i/src/main/java/com/mjolnirr/performance-test/Proxym$i.java
  find proxym$i -type f -exec sed -i "s/roxym{n}/roxym${i}/g" {} \;
  find proxym$i -type f -exec sed -i "s/roxym{m}/roxym${m}/g" {} \;

  cd proxym$i
  mvn clean install
  cp target/proxym$i-0.2-jar*.jar ../jars
  cd ..
done
